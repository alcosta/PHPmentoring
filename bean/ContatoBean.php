<?php

/**
 * Description of ContatoBean
 *
 */
require_once 'model/EmailModel.php';
require_once 'business/ContatoBusiness.php';

class ContatoBean {
    private $nome;
    private $email;
    private $mensagem;
    private $acao;
        
    public function setNome($nome){
        $this->nome = $nome;
    }

    public function getNome(){
        return $this->nome;
    }
    
    public function setEmail($email){
        $this->email = $email;
    }

    public function getEmail(){
        return $this->email;
    }
    
    public function setMensagem($mensagem){
        $this->mensagem = $mensagem;
    }

    public function getMensagem(){
        return $this->mensagem;
    }
       
    public function setAcao($acao){
        $this->acao = $acao;
    }

    public function getAcao(){
        return $this->acao;
    }
    
    public function execute(){
        $contatoBusiness = new ContatoBusiness();
        
        if($this->acao != null){
            if($this->acao == 'Enviar Mensagem'){
               $contatoBusiness->setContato($this->createEmail());
            }
        }        
    }
    
    public function createEmail(){
        $email = new EmailModel($this->nome,$this->email, $this->mensagem);                        
        return $email;
    }
    
}
