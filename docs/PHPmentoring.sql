-- MySQL Script generated by MySQL Workbench
-- Qua 25 Abr 2018 09:44:46 BRT
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema phpuser
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema phpuser
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `phpuser` DEFAULT CHARACTER SET utf8 ;
USE `phpuser` ;

-- -----------------------------------------------------
-- Table `phpuser`.`contato_usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `phpuser`.`contato_usuario` ;

CREATE TABLE IF NOT EXISTS `phpuser`.`contato_usuario` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome_usuario` VARCHAR(80) NOT NULL,
  `email_usuario` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `email_usuario_UNIQUE` (`email_usuario` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `phpuser`.`msg_enviada`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `phpuser`.`msg_enviada` ;

CREATE TABLE IF NOT EXISTS `phpuser`.`msg_enviada` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `mensagem` VARCHAR(200) NOT NULL,
  `data_envio` DATETIME NOT NULL,
  `ip_emissor` VARCHAR(15) NOT NULL,
  `id_usuario` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_msg_enviada_contato_usuario_idx` (`id_usuario` ASC),
  CONSTRAINT `fk_msg_enviada_contato_usuario`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `phpuser`.`contato_usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
