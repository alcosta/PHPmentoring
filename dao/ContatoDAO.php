<?php

/**
 * Description of ContatoDAO
 *
 */
class ContatoDAO { 
    public function addContato(ContatoModel $contato) {
        try {
            $sql="INSERT INTO `contato_usuario` (`nome_usuario`, `email_usuario`) "
               . "VALUES (?, ?)";
            $stmt=ConexaoDB::getConexaoDB()->prepare($sql);
            $stmt->bindValue(1,$contato->getNome());
            $stmt->bindValue(2,$contato->getEmail());
            $stmt->execute(); 
            $id = ConexaoDB::getConexaoDB()->lastInsertId();
            return $id;
        }catch (PDOException $pdo) {
            if ($pdo->errorInfo[1] == 1062){
                $id = $this->getContatoId($contato);
                return $id;
            } else {
                throw new PDOException($pdo->getMessage());         
            } 
        }
    }
    
    private function getContatoId(ContatoModel $contato) {
        try {
            $sql="SELECT `contato_usuario`.`id` FROM `contato_usuario` "
                ."WHERE `contato_usuario`.`email_usuario` = ?";
            $stmt=ConexaoDB::getConexaoDB()->prepare($sql);
            $stmt->bindValue(1,$contato->getEmail());
            $stmt->execute();    
            $result = $stmt -> fetch();
            $id = $result ["id"];
            return $id;             
        }catch (PDOException $pdo) {
            throw new PDOException($pdo->getMessage());          
        }
    } 
}
