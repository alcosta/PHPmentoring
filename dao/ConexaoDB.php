<?php

class ConexaoDB {

    protected static $instance;
        
    private function __construct(){               
        $db_driver = "mysql";  
        $db_host = "10.202.70.136";
        $db_nome = "phpuser";
        $db_usuario = "root";
        $db_senha = "unik2014"; 

        try {
            self::$instance = new PDO("$db_driver:host=$db_host;dbname=$db_nome", $db_usuario, $db_senha);
            self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$instance->exec('SET NAMES utf8');
        }catch (PDOException $pdo) {
            throw new PDOException($pdo->getMessage());
        }    
    }  
    
    public static function getConexaoDB(){
        if (!isset(self::$instance)) {
            try {
                new ConexaoDB();
            }catch (PDOException $pdo) {
                throw new PDOException($pdo->getMessage());
            }     
        }   return self::$instance;
    }  
}
