<?php

/**
 * Description of ContatoDao
 *
 */
class MensagemDAO { 
    public function addMensagem(MensagemModel $mensagem) {
        try {
            $sql="INSERT INTO `msg_enviada` (`mensagem`, `data_envio`, `ip_emissor`, `id_usuario`) "
                ."VALUES (?, CURRENT_TIMESTAMP(), ?, ?)";
            $stmt=ConexaoDB::getConexaoDB()->prepare($sql);
            $stmt->bindValue(1,$mensagem->getMensagem());
            $stmt->bindValue(2,$mensagem->getIpEmissor());
            $stmt->bindValue(3,$mensagem->getIdContato());
            $stmt->execute();
            return "Contato cadastrato com sucesso";
        }catch (PDOException $pdo) {
            throw new PDOException($pdo->getMessage());  
        }
    }
}