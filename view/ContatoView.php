<!DOCTYPE html>
<?php
    require_once 'bean/ContatoBean.php';
    $error= '';
    $sucesso= '';
    $contatoBean = new ContatoBean();  
    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);       
    try { 
        if(isset($post["nome"])){
            $contatoBean->setNome($post["nome"]);
            $contatoBean->setEmail($post["email"]);
            $contatoBean->setMensagem($post["mensagem"]); 
            $contatoBean->setAcao($post["acao"]);
        }
        if ($contatoBean->getAcao()=='Enviar Mensagem'){
            $contatoBean->execute();
            $nome="";
            $email="";
            $mensagem="";
            $sucesso="Sucesso";
        }        
    } catch(PDOException $pdo) {
        $nome=$contatoBean->getNome();
        $email=$contatoBean->getEmail();
        $mensagem=$contatoBean->getMensagem();      
        $error=$pdo->getMessage();
    } catch(Exception $e) {
        $nome=$contatoBean->getNome();
        $email=$contatoBean->getEmail();
        $mensagem=$contatoBean->getMensagem(); 
        $error=$e->getMessage();
    }     
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form method="post" action="index.php">
            <table>
                <tr>
                    <td>
                        <table class="enclasstre" cellpadding="0" cellspacing="0">
                            <tr>  
                                <td><input  name="nome" type="text" id="nome" max-length="80" placeholder="Nome" value="<?php echo $nome;?>"/></td>
                            </tr> 
                            <tr>  
                                <td><input name="email" type="text" id="email" max-length="100" placeholder="Email" value="<?php echo $email;?>"/></td>
                            </tr>
                            <tr>  
                                <td><input name="mensagem" type="text" id="mensagem" max-length="200" placeholder="Mensagem" value="<?php echo $mensagem;?>"/></td>
                            </tr>
                            <tr>
                                <td>
                                    <?PHP echo($error); 
                                          echo($sucesso);
                                    ?>
                                </td>     
                            </tr>
                            <tr>
                                <td><input type="submit" name="acao" class="submit" value="Enviar Mensagem" /></td>
                            </tr>
                        </table>           
                    </td>
                </tr>     
            </table>     
       </form>
    </body>
</html>
