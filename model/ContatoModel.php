<?php

/**
 * Description of Contato
 *
 */
class ContatoModel {
    private $idContato;
    private $nome;
    private $email;
    
    public function __construct($nome,$email){
        $this->nome = $nome;
        $this->email = $email;
    }
    
    public function setIdContato($idContato){
        $this->idContato = $idContato;
    }

    public function getIdContato(){
        return $this->idContato;
    }
    
    public function setNome($nome){
        $this->nome = $nome;
    }

    public function getNome(){
        return $this->nome;
    }
    
    public function setEmail($email){
        $this->email = $email;
    }

    public function getEmail(){
        return $this->email;
    }
}
