<?php

/**
 * Description of Mensagem
 *
 */
class MensagemModel {
    private $mensagem;
    private $ipEmissor;
    private $idContato;
	      
    public function __construct($mensagem,$ipEmissor,$idContato){
        $this->mensagem = $mensagem;
        $this->ipEmissor = $ipEmissor;
	$this->idContato = $idContato;
    }
    
    public function setMensagem($mensagem){
        $this->mensagem = $mensagem;
    }

    public function getMensagem(){
        return $this->mensagem;
    }
       
    public function setIpEmissor($ipEmissor){
        $this->ipEmissor = $ipEmissor;
    }

    public function getIpEmissor(){
        return $this->ipEmissor;
    }
	
    public function setIdContato($idContato){
        $this->idContato = $idContato;
    }

    public function getIdContato(){
        return $this->idContato;
    }
}

