<?php

class EmailModel {
    private $nome;
    private $email;
    private $mensagem;
    
    public function __construct($nome,$email,$mensagem){
        $this->nome = $nome;
        $this->email = $email;
        $this->mensagem = $mensagem;
    }
    
    public function setNome($nome){
        $this->nome = $nome;
    }

    public function getNome(){
        return $this->nome;
    }
    
    public function setEmail($email){
        $this->email = $email;
    }

    public function getEmail(){
        return $this->email;
    }
    
    public function setMensagem($mensagem){
        $this->mensagem = $mensagem;
    }

    public function getMensagem(){
        return $this->mensagem;
    }
}
