<?php

require_once("phpmailer/class.phpmailer.php");

class SendEmail {
    
    private static function send(EmailModel $email){ 
        $mail = new PHPMailer(true);
        $mail->IsSMTP();      
        $to = $email->getEmail();
        $nome = $email->getNome();
        $subject= 'Agradecimento pelo contato';    
        
        $html = file_get_contents('./utils/template-email.php');
                       
        $html = str_replace("[NOME]", $nome,$html);
        $html = str_replace("[SUBJECT]", $subject,$html);
        
        try {           

            $mail->Host = 'smtp.mailgun.org';
            $mail->SMTPSecure = "tls";
            $mail->SMTPAuth = true;
            $mail->Username = 'postmaster@overmail.com.br';
            $mail->Password = 'cfe85babec20ce2ed2088dead52a0cbd';
            $mail->SetLanguage('br');
            $mail->IsHTML(true);           
            $mail->From = "contratante@unik.com.br";
            $mail->FromName = utf8_decode('PHP email');
            $mail->AddAddress($to, 'Teste');         
            $mail->Subject = utf8_decode($subject);                       
            $mail->Body = utf8_decode($html);          
            $mail->AltBody = utf8_decode($subject);           
                       
            $mail->Send();
            echo "Mensagem enviada com sucesso</p>\n";
            
        }catch (phpmailerException $e) {
            throw new Exception($e->getMessage());
        }
        
    }
          
    public static function enviarEmail(EmailModel $email){
        try { 
            self::send($email);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }   
}

