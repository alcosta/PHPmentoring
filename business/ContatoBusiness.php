<?php

require_once 'model/ContatoModel.php';
require_once 'model/MensagemModel.php';
require_once 'dao/ConexaoDB.php';
require_once 'dao/ContatoDAO.php';
require_once 'dao/MensagemDAO.php';
require_once 'utils/SendEmail.php';

class ContatoBusiness {
    private function validarEmail(EmailModel $email){
        if (empty($email->getNome())) {
            throw new Exception("Campo Nome obrigatório");
        }
        if (strlen($email->getNome()) > 80) {
            throw new Exception("Campo Nome passou do limite");
        }       
        if (empty($email->getEmail())) {
            throw new Exception("Campo Email obrigatório");
        }
        if (strlen($email->getEmail()) > 100) {
            throw new Exception("Campo Email passou do limite");
        }
        if (empty($email->getMensagem())) {
            throw new Exception("Campo Mensagem obrigatório");
        }
        if (strlen($email->getMensagem()) > 200) {
            throw new Exception("Campo Mensagem passou do limite");
        }
        if (!preg_match("^([0-9,a-z,A-Z]+)([.,_]([0-9,a-z,A-Z]+))*[@]([0-9,a-z,A-Z]+)([.,_,-]([0-9,a-z,A-Z]+))*[.]([0-9,a-z,A-Z]){2}([0-9,a-z,A-Z])?$^", $email->getEmail())){ 
            throw new Exception("Digite um email valido");
        }      
        
    }
       
    public function setContato(EmailModel $email){
        try {
            $ip = $_SERVER["REMOTE_ADDR"];
            $this->validarEmail($email);     
            $contatoDAO = new ContatoDAO();  
            $contato = new ContatoModel($email->getNome(),$email->getEmail());
            $contato->setIdContato($contatoDAO->addContato($contato));
            $mensagem = new MensagemModel($email->getMensagem(),$ip,$contato->getIdContato());                  
            $mensagemDAO = new MensagemDAO();
            $mensagemDAO->addMensagem($mensagem);
            SendEmail::enviarEmail($email);
        }catch (PDOException $pdo) {
            throw new PDOException($pdo->getMessage());
        }catch (Exception $e) {
            throw new Exception($e->getMessage());
        }          
    }
}
